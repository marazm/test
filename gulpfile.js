var gulp = require('gulp');
var concat = require('gulp-concat');
var less = require('gulp-less');
var watch = require('gulp-watch');

var LessPluginAutoPrefix = require('less-plugin-autoprefix');
var LessPluginCleanCSS = require('less-plugin-clean-css');

var autoprefix = new LessPluginAutoPrefix({ browsers: ["last 2 versions"] });
var cleancss = new LessPluginCleanCSS({ advanced: true });

var styles = 'project/static/styles/main.less';
var scripts = 'project/static/scripts/*.js';

gulp.task('styles', function () {
    return gulp.src(styles)
        .pipe(less({
            plugins: [autoprefix, cleancss]
        }))
        .pipe(gulp.dest('./project/static/build/'));
});

gulp.task('scripts', function () {
    return gulp.src(scripts)
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./project/static/build/'));
});


gulp.task('default', ['styles', 'scripts']);


gulp.task('watch', ['default'], function () {
    watch('project/static/styles/*.less', function () {
        gulp.start('styles');
    });
    watch('project/static/scripts/*.js', function () {
        gulp.start('scripts');
    });
});