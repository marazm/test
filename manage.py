#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask.ext.script import Manager

from project import app
from project.database import init_db, db_session

from project.models import Product

manager = Manager(app)

example_products = [
    {
        'title': u"Картошка",
        'price': '100.00'
    },
    {
        'title': u"Бананы",
        'price': '120.00'
    },
    {
        'title': u"Сосиски",
        'price': '240.00'
    },
    {
        'title': u"Пиво",
        'price': '150.00'
    }
]


@manager.command
def init():
    init_db()
    for item in example_products:
        p = Product(**item)
        db_session.add(p)
    db_session.commit()

if __name__ == "__main__":
    manager.run()
