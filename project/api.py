# -*- coding: utf-8 -*-
from flask import request
from flask_restful import Resource, fields, marshal, marshal_with
from project import api
from project.database import db_session
from project.models import Product, Order

# список полей для сериализации товара
product_fields = {
    'id': fields.Integer,
    'title': fields.String,
    'price': fields.Float
}

# поля для сериализации заказа
order_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'phone': fields.String,
    'date': fields.DateTime,
    'closed': fields.Boolean,
    'product_list': fields.List(fields.Nested(product_fields)),
}


class Catalog(Resource):
    """ API списка товаров.
    """
    def get(self):
        qs = db_session.query(Product).all()
        result = []
        for item in qs:
            result.append(marshal(item, product_fields))
        return result


class OrderListCreate(Resource):
    """ API списка заказов.

    Позволяет POST для создания нового заказа.
    """
    def get(self):
        qs = db_session.query(Order).filter(Order.closed == False)  # noqa
        result = []
        for item in qs:
            result.append(marshal(item, order_fields))
        return result

    @marshal_with(order_fields)
    def post(self):
        data = request.json
        order = Order(name=data.get('name'), phone=data.get('phone'))
        for product in data.get('product_list'):
            p = db_session.query(Product).get(product.get('id'))
            order.product_list.append(p)
        db_session.add(order)
        db_session.commit()
        return order


class CloseOrder(Resource):
    """ Закрытие заказа.

    При POST закрывает указанный аргументом `order_id` заказ.
    """
    @marshal_with(order_fields)
    def post(self, order_id):
        order = db_session.query(Order).get(order_id)
        order.closed = True
        order.name = request.json.get('name')
        order.phone = request.json.get('phone')
        db_session.add(order)
        db_session.commit()
        return order


class OrderDetail(Resource):
    """ Подробности заказа.
    """
    @marshal_with(order_fields)
    def get(self, order_id):
        order = db_session.query(Order).get(order_id)
        return order


api.add_resource(Catalog, '/api/catalog/catalog.json')

api.add_resource(OrderListCreate, '/api/orders/')
api.add_resource(OrderDetail, '/api/orders/<int:order_id>')
api.add_resource(CloseOrder, '/api/orders/<int:order_id>/close')
