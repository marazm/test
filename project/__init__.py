# -*- coding: utf-8 -*-
from flask import Flask
from flask_restful import Api

from project.database import db_session

app = Flask(__name__)
app.debug = True

api = Api(app)

import project.api
import project.views

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()
