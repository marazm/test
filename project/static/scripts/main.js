var orderApp = angular.module('orderApp', ['ngRoute', 'ngResource', 'alertBox', 'orderAdmin', 'catalog']);


orderApp.config(['$routeProvider',
    function ($routeProvider) {
        'use strict';
        $routeProvider.
            when('/product-list', {
                templateUrl: '/static/partials/product-list.html',
                controller: 'ProductListCtrl'
            }).
            when('/order', {
                templateUrl: '/static/partials/order.html',
                controller: 'OrderCtrl'
            }).
            when('/order-list', {
                templateUrl: '/static/partials/order-list.html',
                controller: 'OrderListCtrl'
            }).
            when('/order-detail/:orderId', {
                templateUrl: '/static/partials/order-detail.html',
                controller: 'OrderDetailCtrl'
            }).
                otherwise({
                redirectTo: '/product-list'
            });
    }]);
