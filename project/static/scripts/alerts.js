angular.module('alertBox', [])

.controller('AlertController', ['$scope', '$attrs', function ($scope, $attrs) {
  $scope.closeable = !!$attrs.close;
  this.close = $scope.close;
}])

.directive('alert', function () {
  return {
    restrict:'EA',
    controller:'AlertController',
    templateUrl:'/static/partials/alert.html',
    transclude:true,
    replace:true,
    scope: {
      type: '@',
      close: '&'
    }
  };
})

.directive('dismissOnTimeout', ['$timeout', function($timeout) {
  return {
    require: 'alert',
    link: function(scope, element, attrs, alertCtrl) {
      $timeout(function(){
        alertCtrl.close();
      }, parseInt(attrs.dismissOnTimeout, 10));
    }
  };
}])


.factory('messages', ['$rootScope', '$timeout', function ($rootScope, $timeout) {
    $rootScope.messages = [];

    $rootScope.closeAlert = function (i) {
        $rootScope.messages.splice(i, 1);
    }

    return {
        send: function (message) {
            $rootScope.messages.push(message);
        },
        flush: function () {
            $rootScope.messages = [];
        }
    };
}]);