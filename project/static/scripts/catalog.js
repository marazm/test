angular.module('catalog', ['alertBox', 'cart'])
    .factory('Product', ['$resource',
        // Простой ресурс товара
        function ($resource) {
            'use strict';
            return $resource('/api/catalog/:productId.json', {}, {
                query: {
                    method: 'GET',
                    params: {productId: 'catalog'},
                    isArray: true
                }
            });
        }])
    .controller('ProductListCtrl',
        // Список товаров
        function ($scope, $rootScope, Product, Cart) {
            'use strict';
            $scope.productList = Product.query();

            $scope.addToCart = function (item) {
                // Добавление указанного товара в корзину
                $rootScope.cartContent.push(item);

                Cart.update();
            };

            $scope.notInCart = function (item) {
                // фильтр для списка товаров в каталоге, исключающий из него
                // товары, находящиеся в корзине.
                var i, cart_item;
                for (i = $scope.cartContent.length - 1; i >= 0; i--) {
                    cart_item = $scope.cartContent[i];
                    if (cart_item.id === item.id) {
                        return false;
                    }
                }
                return true;
            };
        })
    .controller('OrderCtrl', function ($scope, $rootScope, $location, $http, messages, Cart) {
        // Подтверждение заказа
        'use strict';

        $scope.finishOrder = function () {
            if ($scope.OrderDetail.$valid) {
                var order_data = {
                    name: $scope.userName,
                    phone: $scope.userPhone,
                    product_list: $scope.cartContent
                };

                // TODO: использовать ресурс Order
                $http.post('/api/orders/', order_data).then(function (response) {
                    messages.send("Заказ #" + response.data.id + " успешно оформлен");
                    $rootScope.cartContent = [];
                    $location.path('/');

                    localStorage.setItem('cartContent', '[]');
                }, function (response) {
                    messages.send("Произошла ошибка");
                });
            } else {
                messages.send("Исправьте ошибки заполнения формы");
            }
        };
    });