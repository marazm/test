angular.module('cart', [])
    .directive('cart', function () {
        return {
            templateUrl: '/static/partials/cart.html',
            restrict: 'E',
            controller: 'CartController',
            scope: {
                productList: '=objects'
            }
        }
    })
    .controller('CartController', ['$scope', '$rootScope', 'Cart', function ($scope, $rootScope, Cart) {
        $scope.removeFromCart = function (item) {
            // Удаление товара из корзины
            var i = $rootScope.cartContent.indexOf(item);
            if (i > -1) {
              $rootScope.cartContent.splice(i, 1);
            }

            Cart.update();
        };
    }])
    .factory('Cart', ['$rootScope', function ($rootScope) {
        // Корзина
        // Загружает в scope данные из storage и предоставляет метод для
        // обновления этих данных в хранилище.
        // TODO: fallback до сессий
        $rootScope.cartContent = localStorage.getItem('cartContent');
        if ($rootScope.cartContent === undefined) {
            $rootScope.cartContent = [];
        } else {
            $rootScope.cartContent = JSON.parse($rootScope.cartContent);
        }
        return {
            update: function () {
                var data = JSON.stringify($rootScope.cartContent);
                localStorage.setItem('cartContent', data);
            }
        };
    }])
