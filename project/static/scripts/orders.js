angular.module('orderAdmin', ['cart'])
    .directive('validPhone', function () {
        'use strict';
        var phone_regexp = /[0-9\-\(\s\+]{10,18}/i;
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                ctrl.$parsers.unshift(function(value) {
                    if (phone_regexp.test(value)) {
                        ctrl.$setValidity('phone', true);
                        return value;
                    } else {
                        ctrl.$setValidity('phone', false);
                        return undefined;
                    }
                });
            }
        };
    })

    .filter("asDate", function () {
        return function (input) {
            return new Date(input);
        };
    })

    .factory('Order', ['$resource',
        function ($resource) {
            'use strict';
            return $resource('/api/orders/:orderId', {orderId: '@id'}, {
                query: {method: 'GET', isArray: true},
                close: {method: 'POST', url: '/api/orders/:orderId/close'}
            });
        }])

    .controller('OrderListCtrl', function ($scope, Order, $location) {
        'use strict';
        $scope.orderList = Order.query();

        $scope.showDetails = function (item) {
            $location.path('/order-detail/' + item.id);
        };
    })


    .controller('OrderDetailCtrl', function ($scope, $routeParams, Order, $location, messages) {
        $scope.order = Order.get({orderId: $routeParams.orderId}, function (obj) {
            $scope.order = obj;
        });

        $scope.closeOrder = function (order) {
            messages.send("Заказ " + order.id + " закрыт");
            order.$close();
            $location.path('/order-list/');
        };
    });