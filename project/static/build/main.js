angular.module('alertBox', [])

.controller('AlertController', ['$scope', '$attrs', function ($scope, $attrs) {
  $scope.closeable = !!$attrs.close;
  this.close = $scope.close;
}])

.directive('alert', function () {
  return {
    restrict:'EA',
    controller:'AlertController',
    templateUrl:'/static/partials/alert.html',
    transclude:true,
    replace:true,
    scope: {
      type: '@',
      close: '&'
    }
  };
})

.directive('dismissOnTimeout', ['$timeout', function($timeout) {
  return {
    require: 'alert',
    link: function(scope, element, attrs, alertCtrl) {
      $timeout(function(){
        alertCtrl.close();
      }, parseInt(attrs.dismissOnTimeout, 10));
    }
  };
}])


.factory('messages', ['$rootScope', '$timeout', function ($rootScope, $timeout) {
    $rootScope.messages = [];

    $rootScope.closeAlert = function (i) {
        $rootScope.messages.splice(i, 1);
    }

    return {
        send: function (message) {
            $rootScope.messages.push(message);
        },
        flush: function () {
            $rootScope.messages = [];
        }
    };
}]);
angular.module('cart', [])
    .directive('cart', function () {
        return {
            templateUrl: '/static/partials/cart.html',
            restrict: 'E',
            controller: 'CartController',
            scope: {
                productList: '=objects'
            }
        }
    })
    .controller('CartController', ['$scope', '$rootScope', 'Cart', function ($scope, $rootScope, Cart) {
        $scope.removeFromCart = function (item) {
            // Удаление товара из корзины
            var i = $rootScope.cartContent.indexOf(item);
            if (i > -1) {
              $rootScope.cartContent.splice(i, 1);
            }

            Cart.update();
        };
    }])
    .factory('Cart', ['$rootScope', function ($rootScope) {
        // Корзина
        // Загружает в scope данные из storage и предоставляет метод для
        // обновления этих данных в хранилище.
        // TODO: fallback до сессий
        $rootScope.cartContent = localStorage.getItem('cartContent');
        if ($rootScope.cartContent === undefined) {
            $rootScope.cartContent = [];
        } else {
            $rootScope.cartContent = JSON.parse($rootScope.cartContent);
        }
        return {
            update: function () {
                var data = JSON.stringify($rootScope.cartContent);
                localStorage.setItem('cartContent', data);
            }
        };
    }])

angular.module('catalog', ['alertBox', 'cart'])
    .factory('Product', ['$resource',
        // Простой ресурс товара
        function ($resource) {
            'use strict';
            return $resource('/api/catalog/:productId.json', {}, {
                query: {
                    method: 'GET',
                    params: {productId: 'catalog'},
                    isArray: true
                }
            });
        }])
    .controller('ProductListCtrl',
        // Список товаров
        function ($scope, $rootScope, Product, Cart) {
            'use strict';
            $scope.productList = Product.query();

            $scope.addToCart = function (item) {
                // Добавление указанного товара в корзину
                $rootScope.cartContent.push(item);

                Cart.update();
            };

            $scope.notInCart = function (item) {
                // фильтр для списка товаров в каталоге, исключающий из него
                // товары, находящиеся в корзине.
                var i, cart_item;
                for (i = $scope.cartContent.length - 1; i >= 0; i--) {
                    cart_item = $scope.cartContent[i];
                    if (cart_item.id === item.id) {
                        return false;
                    }
                }
                return true;
            };
        })
    .controller('OrderCtrl', function ($scope, $rootScope, $location, $http, messages, Cart) {
        // Подтверждение заказа
        'use strict';

        $scope.finishOrder = function () {
            if ($scope.OrderDetail.$valid) {
                var order_data = {
                    name: $scope.userName,
                    phone: $scope.userPhone,
                    product_list: $scope.cartContent
                };

                // TODO: использовать ресурс Order
                $http.post('/api/orders/', order_data).then(function (response) {
                    messages.send("Заказ #" + response.data.id + " успешно оформлен");
                    $rootScope.cartContent = [];
                    $location.path('/');

                    localStorage.setItem('cartContent', '[]');
                }, function (response) {
                    messages.send("Произошла ошибка");
                });
            } else {
                messages.send("Исправьте ошибки заполнения формы");
            }
        };
    });
var orderApp = angular.module('orderApp', ['ngRoute', 'ngResource', 'alertBox', 'orderAdmin', 'catalog']);


orderApp.config(['$routeProvider',
    function ($routeProvider) {
        'use strict';
        $routeProvider.
            when('/product-list', {
                templateUrl: '/static/partials/product-list.html',
                controller: 'ProductListCtrl'
            }).
            when('/order', {
                templateUrl: '/static/partials/order.html',
                controller: 'OrderCtrl'
            }).
            when('/order-list', {
                templateUrl: '/static/partials/order-list.html',
                controller: 'OrderListCtrl'
            }).
            when('/order-detail/:orderId', {
                templateUrl: '/static/partials/order-detail.html',
                controller: 'OrderDetailCtrl'
            }).
                otherwise({
                redirectTo: '/product-list'
            });
    }]);

angular.module('orderAdmin', ['cart'])
    .directive('validPhone', function () {
        'use strict';
        var phone_regexp = /[0-9\-\(\s\+]{10,18}/i;
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                ctrl.$parsers.unshift(function(value) {
                    if (phone_regexp.test(value)) {
                        ctrl.$setValidity('phone', true);
                        return value;
                    } else {
                        ctrl.$setValidity('phone', false);
                        return undefined;
                    }
                });
            }
        };
    })

    .filter("asDate", function () {
        return function (input) {
            return new Date(input);
        };
    })

    .factory('Order', ['$resource',
        function ($resource) {
            'use strict';
            return $resource('/api/orders/:orderId', {orderId: '@id'}, {
                query: {method: 'GET', isArray: true},
                close: {method: 'POST', url: '/api/orders/:orderId/close'}
            });
        }])

    .controller('OrderListCtrl', function ($scope, Order, $location) {
        'use strict';
        $scope.orderList = Order.query();

        $scope.showDetails = function (item) {
            $location.path('/order-detail/' + item.id);
        };
    })


    .controller('OrderDetailCtrl', function ($scope, $routeParams, Order, $location, messages) {
        $scope.order = Order.get({orderId: $routeParams.orderId}, function (obj) {
            $scope.order = obj;
        });

        $scope.closeOrder = function (order) {
            messages.send("Заказ " + order.id + " закрыт");
            order.$close();
            $location.path('/order-list/');
        };
    });