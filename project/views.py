# -*- coding: utf-8 -*-
from flask import make_response

from project import app


@app.route('/')
def basic_pages(**kwargs):
    return make_response(open('project/templates/index.html').read())
