# -*- coding: utf-8 -*-
import datetime

from sqlalchemy import Column, Table
from sqlalchemy import String, Integer, DateTime, Numeric, ForeignKey, Boolean
from sqlalchemy.orm import relationship

from project.database import Base


class Product(Base):
    __tablename__ = 'product'

    id = Column(Integer(), primary_key=True)
    title = Column(String(50))
    price = Column(Numeric())


order_items = Table(
    'order_items', Base.metadata,
    Column('product_id', Integer, ForeignKey('product.id')),
    Column('order_id', Integer, ForeignKey('order.id'))
)


class Order(Base):
    __tablename__ = 'order'

    id = Column(Integer(), primary_key=True)
    date = Column(DateTime(), default=datetime.datetime.utcnow)
    name = Column(String(50))
    phone = Column(String(15))
    closed = Column(Boolean(), default=False)

    product_list = relationship(Product, secondary=order_items, lazy='joined')
